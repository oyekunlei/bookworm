﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teljoy_Assesment.Models.Database
{
    public class BookWormRepository 
    {
        string connectionString = @"Data Source=ODW0132232\SQLSERVER;Initial Catalog=PlayboyPlayer;Integrated Security=True";

        public int InsertWordFrequencies(Dictionary<string, int> wordFrequencies)
        {
            SqlTransaction transaction = null;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    transaction = conn.BeginTransaction();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[InsertWordFrequency]", conn, transaction))
                    {
                        foreach (KeyValuePair<string, int> wordFrequency in wordFrequencies)
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            var sqlParameters = new[]
                            {
                                new SqlParameter("@Word", SqlDbType.NVarChar){Value = wordFrequency.Key },
                                new SqlParameter("@Frequency", SqlDbType.NVarChar){Value = wordFrequency.Value }
                            };
                            cmd.Parameters.AddRange(sqlParameters.ToArray());
                            cmd.ExecuteNonQuery();
                        }
                        transaction.Commit();
                    }
                };

                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
