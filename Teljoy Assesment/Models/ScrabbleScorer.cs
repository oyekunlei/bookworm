﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Teljoy_Assesment.Models
{
    public class ScrabbleScorer
    {
        private List<string> Words { get; set; }

        public ScrabbleScorer(List<string> words)
        {
            Words = words;
        }
        private int GetLetterScore(char letter)
        {
            //Map letters to scores using a dictionary
            var letterScores = new Dictionary<char, int>
            {
                {'a', 1 }, {'e', 1 }, {'i', 1 }, {'l', 1 }, {'n', 1 }, {'o', 1 }, {'r', 1 }, {'s', 1 }, {'t', 1 }, {'u', 1 },
                {'d', 2 }, {'g', 2 },
                {'b', 3 }, {'c', 3 }, {'m', 3 }, {'p', 3 },
                {'f', 4 }, {'h', 4 }, {'v', 4 }, {'w', 4 }, {'y', 4 },
                {'k', 5 },
                {'j', 8 }, {'x', 8 },
                {'q', 10 }, {'z', 10 },
            };
            
            //Check if character is in the dictionary before checking its score or return a 0 for an invalid character
            return letterScores.ContainsKey(letter) ? letterScores[letter] : 0;
        }

        private int GetWordScore(string word)
        {
            //Get the score for each letter and sum it up
            return word.ToCharArray().Select(x => GetLetterScore(x)).Sum();
        }

        private Dictionary<string, int> GetScrabbleScore()
        {
            Dictionary<string, int> scrabbleScores = new Dictionary<string, int>();

            //Get the score for all the words in the dictionary
            Words.ForEach(x => scrabbleScores[x] = GetWordScore(x));

            return scrabbleScores;
        }

        public string GetHighestScoringScrabbleWord()
        {
            Dictionary<string, int> scrabbleScores = GetScrabbleScore();

            //Order the dictionary in descending order from the highest to the lowest
            var sortedFrequencies = (from scrabbleScore in scrabbleScores
                                     orderby scrabbleScore.Value descending
                                     select scrabbleScore)
                                     .ToDictionary(pair => pair.Key, pair => pair.Value);

            //Return the entry at the top of the list which will be the highest score 
            var top = sortedFrequencies.First();

            //Return string indicating the word with the highest scrabble score
            return $"Highest scoring word (according to Scrabble): {top.Key} with a score of {top.Value}";
        }
    }
}