﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Teljoy_Assesment.Models;
using Teljoy_Assesment.Models.Database;

namespace Teljoy_Assesment.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string url)
        {
            //Instantiate the Bookworm and pass it the url of the book
            BookWorm bookworm = new BookWorm(url);

            //Get a list of all the words in the book
            List<string> words = bookworm.GetWordFrequencyDictionary().Keys.ToList();

            //Insert tuple <Word, Frequency> into the database
            BookWormRepository repo = new BookWormRepository();
            repo.InsertWordFrequencies(bookworm.GetWordFrequencyDictionary());

            //Instantiate the Scrabble scorer and pass it the list of words
            ScrabbleScorer scrabbleScorer = new ScrabbleScorer(words);

            //Return required data to the view
            return Content($"{bookworm.GetMostFrequentWord()} <br/> {bookworm.GetMostFrequentSevenCharacterWord()} <br/> {scrabbleScorer.GetHighestScoringScrabbleWord()}");
        }
    }
}