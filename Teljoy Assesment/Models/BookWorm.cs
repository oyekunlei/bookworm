﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace Teljoy_Assesment.Models
{
    public class BookWorm
    {
        private string Url { get; set; }
        private Dictionary<string, int> WordFrequencyDictionary { get; set; }

        public BookWorm(string url)
        {
            Url = url;
        }

        private string ReadBookData()
        {
            StringBuilder bookText = new StringBuilder();

            //Read remote text file into a string builder
            using (WebClient client = new WebClient())
            {
                using (Stream stream = client.OpenRead(Url))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string line;

                        while ((line = reader.ReadLine()) != null)
                        {
                            bookText.Append(line + " ");
                        }
                    }
                }
            }
            return bookText.ToString();
        }

        private string StripBookDataOfCharacters()
        {
            var bookData = ReadBookData();
            //Define caharcters to remove from the string
            string[] characters = { ";", ",", ".", "-", "_", "^", "(", ")", "[", "]", "{", "}", ":", "\"", "”", "/","*",
                                    "?", "<", ">", "\\", "=", "&", "%", "$", "#", "@", "!", "~", "+", "—", "“", "’",
                                    "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" , Environment.NewLine, "\t", "\r" };

            //Replace all unwanted characters with an empty string
            characters.ToList().ForEach(x => bookData = bookData.Replace(x, " "));

            //Convert string to lower case
            return bookData.ToLower();
        }

        public Dictionary<string, int> GetWordFrequencyDictionary()
        {
            var bookData = StripBookDataOfCharacters();
            //Split string at the spaces
            var words = bookData.Split(' ').ToList();
            Dictionary<string, int> wordFrequencyDictionary = new Dictionary<string, int>();

            //Create word frequency dictionary
            foreach(var word in words)
            {
                if(!word.Equals(" ") && !word.Equals("")){
                    if (wordFrequencyDictionary.ContainsKey(word))
                    {
                        //If word is already in the dictionary then increment the frequency value
                        wordFrequencyDictionary[word]++;
                    }
                    else
                    {
                        //If word is new in the dictionary add the word and set value to 1
                        wordFrequencyDictionary[word] = 1;
                    }
                }
            }
            WordFrequencyDictionary =  wordFrequencyDictionary;
            return wordFrequencyDictionary;
        }

        private KeyValuePair<string, int> GetMostFrequentWordHelper(Dictionary<string, int> wordFrequencies)
        {
            //Order the dictionary by frequencies from the highest to the lowest
            var sortedFrequencies = (from wordFrequency in wordFrequencies
                                     orderby wordFrequency.Value descending
                                     select wordFrequency)
                                     .ToDictionary(pair => pair.Key, pair => pair.Value);

            //Return the entry at the top of the list which will be the highest frequency
            return sortedFrequencies.First();
        }

        public string GetMostFrequentWord()
        {
            //Return string indicating the word with the hishest frequency
            var mostFreqent = GetMostFrequentWordHelper(WordFrequencyDictionary);
            return $"Most frequent word: {mostFreqent.Key} occured {mostFreqent.Value} times";
        }

        private Dictionary<string, int> GetSevenCharacterWords()
        {
            //Get all the seven character words in the dictionary
            var sevenCharacterWords = (from wordFrequency in WordFrequencyDictionary
                                       where wordFrequency.Key.Length == 7
                                       select wordFrequency)
                                       .ToDictionary(pair => pair.Key, pair => pair.Value);

            return sevenCharacterWords;
        }

        public string GetMostFrequentSevenCharacterWord()
        {
            var wordFrequencies = GetSevenCharacterWords();
            //Get the 7 character word with the highest frequency
            var mostFreqent = GetMostFrequentWordHelper(wordFrequencies);

            //Return string inidicating the 7 character word with the highest frequency
            return $"Most frequent 7 character word: {mostFreqent.Key} occured {mostFreqent.Value} times";
        }
    }
}